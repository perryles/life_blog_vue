# 使用 Node.js 镜像作为基础镜像
FROM node:14-alpine

# 设置工作目录
WORKDIR /app

# 复制 package.json 和 package-lock.json（如果存在）到工作目录
COPY package*.json ./

# 安装项目依赖
RUN npm install

# 复制所有文件到工作目录
COPY . .

# 构建 Vue.js 项目
RUN npm run build

# 暴露容器的端口
EXPOSE 5173

# 启动应用程序
CMD ["npm", "run", "dev"]
