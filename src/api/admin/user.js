import axios from "@/axios"

export function login(username, password) {
    return axios.post("/login", {username, password}, {
        headers: {
            'Content-Type': 'application/json', // 设置请求体类型
            'Login-Method': 'password' // 设置 loginMethod 到请求头
        }
    })
}

export function getAdminInfo() {
    return axios.post("/admin/detail")
}

export function updateAdminPassword(data) {
    return axios.post("/admin/password/update", data)
}
